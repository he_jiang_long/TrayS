﻿//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ 生成的包含文件。
// 供 TrayS.rc 使用
//
#define IDC_MYICON                      2
#define IDD_TRAYS_DIALOG                102
#define IDS_APP_TITLE                   103
#define IDD_ABOUTBOX                    103
#define IDD_MAIN                        103
#define IDS_TIPS                        104
#define IDI_TRAYS                       107
#define IDC_TRAYS                       109
#define IDR_MAINFRAME                   128
#define IDC_RADIO_DEFAULT               1001
#define IDC_RADIO_TRANSPARENT           1002
#define IDC_RADIO_BLURBEHIND            1003
#define IDC_RADIO_ACRYLIC               1004
#define IDC_BUTTON_COLOR                1005
#define IDC_SLIDER_ALPHA_B              1006
#define IDC_SLIDER_ALPHA                1007
#define IDC_CLOSE                       1008
#define IDC_CHECK_AUTORUN               1009
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1010
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
